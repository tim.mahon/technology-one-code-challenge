using System.Text;
using TechOneCurrencyChallenge.Services.Interfaces;
using TechOneCurrencyChallenge.Util;

namespace TechOneCurrencyChallenge.Services;

public class EnglishNumbersToWordsService : IEnglishNumbersToWordsService
{
	private readonly ILogger<EnglishNumbersToWordsService> _logger;

	private readonly Dictionary<int, string> _ones = new()
	{
		{ 1, "one" },
		{ 2, "two" },
		{ 3, "three" },
		{ 4, "four" },
		{ 5, "five" },
		{ 6, "six" },
		{ 7, "seven" },
		{ 8, "eight" },
		{ 9, "nine" },
	};

	private readonly Dictionary<int, string> _twoDigitWords = new()
	{
		{ 10, "ten" },
		{ 11, "eleven" },
		{ 12, "twelve" },
		{ 13, "thirteen" },
		{ 14, "fourteen" },
		{ 15, "fifteen" },
		{ 16, "sixteen" },
		{ 17, "seventeen" },
		{ 18, "eighteen" },
		{ 19, "nineteen" },
		{ 20, "twenty" },
		{ 30, "thirty" },
		{ 40, "forty" },
		{ 50, "fifty" },
		{ 60, "sixty" },
		{ 70, "seventy" },
		{ 80, "eighty" },
		{ 90, "ninety" },
	};

	private readonly Dictionary<int, string> _tens = new()
	{
		{ 1, "ten" },
		{ 2, "twenty" },
		{ 3, "thirty" },
		{ 4, "forty" },
		{ 5, "fifty" },
		{ 6, "sixty" },
		{ 7, "seventy" },
		{ 8, "eighty" },
		{ 9, "ninety" },
		{ 10, "hundred" },
	};

	private readonly Dictionary<long, string> _powersOfOneThousand = new()
	{
		{ 1000, "thousand" },
		{ 1000000, "million" },
		{ 1000000000, "billion" },
		{ 1000000000000, "trillion" },
		{ 1000000000000000, "quadrillion" },
		{ 1000000000000000000, "quintillion" },
		// any larger would requires values to be written as doubles, and key would need to be a double
		//	in our use case of money this doesn't seem necessary
		//	however, adding subsequent values to this dictionary would achieve this.
	};

	public EnglishNumbersToWordsService(ILogger<EnglishNumbersToWordsService> logger)
	{
		_logger = logger;
	}

	/**
	 * Determines whether a number should be plural and returns the words string appended with an 's'.
	 *
	 * @param value The number to check.
	 * @param word The word which needs to be pluralised.
	 * @return The string representation of the number.
	 */
	private static string Pluralise(long value, string word)
	{
		return $"{word}" + (Math.Abs(value - 1) == 0 ? "" : "s");
	}

	/**
	 * Converts a three digit number to words
	 *
	 * @param number The three digit number to convert
	 * @return The number in words
	 */
	private string ThreeDigitNumberToWords(short number)
	{
		if (number % 1000 == 0)
		{
			return "zero";
		}

		var hundreds = number % 1000 / 100;
		var tens = number % 100 / 10;
		var ones = number % 10;

		string? hundredsWord = null;
		string? tensWord = null;
		string? onesWord = null;

		if (hundreds > 0)
		{
			_ones.TryGetValue(hundreds, out hundredsWord);
		}

		// due to some numbers being named based on two digits, we need to check if this is the case
		if (!_twoDigitWords.TryGetValue(number % 100, out tensWord))
		{
			_tens.TryGetValue(tens, out tensWord);
			_ones.TryGetValue(ones, out onesWord);
		}

		var result = new StringBuilder();
		if (hundredsWord != null)
		{
			result.Append($"{hundredsWord} hundred ");
		}
		if (hundredsWord != null && (tensWord != null || onesWord != null))
		{
			result.Append("and ");
		}
		if (tensWord != null)
		{
			result.Append($"{tensWord} ");
		}
		if (onesWord != null)
		{
			result.Append($"{onesWord} ");
		}

		return result.ToString().TrimEnd();
	}

	/**
	 * Converts a number to words.
	 *
	 * @param number the number to convert
	 * @return the number in words
	 */
	private string NumberToWords(long number)
	{
		var result = new StringBuilder();

		long temp = 1;
		while (number >= temp)
		{
			// The value we are dealing with this iteration
			var section = (short)((number / temp) % 1000);

			var threeDigitNumber = ThreeDigitNumberToWords(section);

			// When iteration multiple powers we do not want add zero unless there are no more sections left
			if (!string.IsNullOrEmpty(threeDigitNumber) && threeDigitNumber != "zero")
			{
				if (_powersOfOneThousand.TryGetValue(temp, out var shortScale))
				{
					result.Prepend($"{shortScale} ");
				}
				result.Prepend($"{threeDigitNumber} ");
			}

			// Grammatical edge case:
			//	IF there are tens and ones but no hundreds
			//	AND it is the first iteration
			//	AND there will be other numbers prepended
			//	THEN, add an "and"
			if (temp == 1 && number % 1000 / 100 < 1 && number / 100 > 1 && number % 100 > 0)
			{
				result.Prepend("and ");
			}
			temp *= 1000;
		}

		// return zero if the result is empty
		return result.Length == 0 ? "zero" : result.ToString().TrimEnd();
	}

	/**
	 * Convert a Dollar amount to words.
	 *
	 * @param value The dollar amount to convert.
	 * @return The dollar amount in words.
	 */
	public async Task<string> ConvertDollarValueToString(double value)
	{
		_logger.LogInformation("Converting dollar value to string");
		var cents = (short)Math.Round((value * 100) % 100);
		// converting to long will remove decimal places
		var dollars = (long)value;

		var pluralisedDollar = Pluralise(dollars, "dollar");
		var pluralisedCent = Pluralise(cents, "cent");

		var centsInWords = ThreeDigitNumberToWords(cents);
		var dollarsInWords = NumberToWords(dollars);

		var result = $"{dollarsInWords} {pluralisedDollar} and {centsInWords} {pluralisedCent}".ToUpper();

		_logger.LogInformation("Got Value: {Result}", result);

		return await Task.FromResult(result);
	}
}
