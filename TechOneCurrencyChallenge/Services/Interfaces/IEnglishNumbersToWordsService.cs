namespace TechOneCurrencyChallenge.Services.Interfaces;

public interface IEnglishNumbersToWordsService
{
	public Task<string> ConvertDollarValueToString(double value);
}
