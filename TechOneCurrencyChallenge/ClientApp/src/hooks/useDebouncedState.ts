import { useEffect, useState } from 'react';

export function useDebouncedState<T>(value: T, delay: number): [T, (newValue: T) => void] {
  const [debouncedValue, setDebouncedValue] = useState(value);

  useEffect(() => {
	const timeout = setTimeout(() => {
	  setDebouncedValue(value);
	}, delay);

	return () => clearTimeout(timeout);
  }, [value, delay]);

  return [debouncedValue, setDebouncedValue];
}
