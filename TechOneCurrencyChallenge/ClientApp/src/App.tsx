import React from 'react';
import { CurrencyLongForm } from './components/CurrencyLongForm';
import './custom.css';

export default function App() {
	return (
		<div className={'app'}>
			<h1>TechnologyOne Code Challenge</h1>
			<CurrencyLongForm />
		</div>
	);
}
