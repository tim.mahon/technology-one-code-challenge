import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { create } from 'react-test-renderer';
import { render, screen, waitFor } from '@testing-library/react'
import userEvent from '@testing-library/user-event'

beforeEach(() => {
  // @ts-ignore
  jest.spyOn(global, 'fetch').mockResolvedValue({
    text: jest.fn().mockResolvedValue("ONE DOLLAR AND ZERO CENTS"),
  });
});

afterEach(() => {
  jest.restoreAllMocks();
});

it('renders without crashing', async () => {
  const div = document.createElement('div');
  ReactDOM.render(<App />, div);
  await new Promise(resolve => setTimeout(resolve, 1000));
});

it('renders correctly', async () => {
  const component = create(<App />).toJSON();
  expect(component).toMatchSnapshot();
});

it('accepts input', async () => {
  render(<App />);
  const input = screen.getByTestId('dollar-numeric-input')
  userEvent.type(input, '100');
  // @ts-ignore
  await waitFor(() => expect(input.value).toBe('100'));
});
