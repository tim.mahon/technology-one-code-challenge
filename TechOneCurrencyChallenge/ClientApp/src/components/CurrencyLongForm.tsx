import React, { useState } from 'react';
import { useDebouncedState } from '../hooks/useDebouncedState';
import useAsyncEffect from '../hooks/useAsyncEffect';

export const CurrencyLongForm: React.FC = () => {
	const [value, setValue] = useState(0);
	const [loading, setLoading] = useState(false);
	const [debouncedValue] = useDebouncedState(value, 500);

	const [currencyLongForm, setCurrencyLongForm] = useState('');

	useAsyncEffect(async () => {
		setLoading(true);
		try {
			const res = await fetch('/api/Currency/ConvertToLongCurrency/' + encodeURIComponent(debouncedValue));
			const data = await res.text();
			setCurrencyLongForm(data);
		} catch (e) {
			setCurrencyLongForm('');
		} finally {
			setLoading(false);
		}
	}, [debouncedValue]);

	const onChange: React.ChangeEventHandler<HTMLInputElement> = (e) => {
		setValue(e.target.valueAsNumber);
	}

	return (
		<div className={'currency-long-form-section'}>
			<input data-testid={'dollar-numeric-input'} type="number" value={value} onChange={onChange} />
			{loading ? <span>Loading...</span> : <span>{currencyLongForm}</span>}
		</div>
	);
}
