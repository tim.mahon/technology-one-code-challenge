﻿using System.Globalization;
using Microsoft.AspNetCore.Mvc;
using TechOneCurrencyChallenge.Services.Interfaces;

namespace TechOneCurrencyChallenge.Controllers;

[ApiController]
[Route("api/[controller]")]
public class CurrencyController : ControllerBase
{

    private readonly ILogger<CurrencyController> _logger;
    private readonly IEnglishNumbersToWordsService _englishNumbersToWordsService;

    public CurrencyController(
        ILogger<CurrencyController> logger,
        IEnglishNumbersToWordsService englishNumbersToWordsService
        )
    {
        _logger = logger;
        _englishNumbersToWordsService = englishNumbersToWordsService;
    }

    [HttpGet]
    [Route("ConvertToLongCurrency/{value:double}")]
    public async Task<string> ConvertToLongCurrency(double value)
    {
        _logger.LogInformation("ConvertToLongCurrency");
        return await _englishNumbersToWordsService.ConvertDollarValueToString(value);
    }
}
