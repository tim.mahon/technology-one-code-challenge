using System.Text;

namespace TechOneCurrencyChallenge.Util;

public static class StringBuilderUtil
{
	public static StringBuilder Prepend(this StringBuilder builder, string text)
	{
		builder.Insert(0, $"{text}");
		return builder;
	}
}
