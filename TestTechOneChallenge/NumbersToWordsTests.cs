using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using TechOneCurrencyChallenge.Services;
using TechOneCurrencyChallenge.Services.Interfaces;
using Xunit;

namespace TestTechOneChallenge;

public class NumbersToWordsTests
{
	private readonly IEnglishNumbersToWordsService _englishNumbersToWordsService;

	public NumbersToWordsTests()
	{
		_englishNumbersToWordsService = new EnglishNumbersToWordsService(new Logger<EnglishNumbersToWordsService>(new LoggerFactory()));
	}

	public class ConvertToWordsTestCase
	{
		public double Input { get; init; }
		public string Expected { get; init; }
	}

	public static TheoryData<ConvertToWordsTestCase> ConvertBaseTheoryData()
	{
		return new()
		{
			new ConvertToWordsTestCase { Input = 1.0, Expected = "ONE DOLLAR AND ZERO CENTS" },
			new ConvertToWordsTestCase { Input = 11.0, Expected = "ELEVEN DOLLARS AND ZERO CENTS" },
			new ConvertToWordsTestCase { Input = 21.0, Expected = "TWENTY ONE DOLLARS AND ZERO CENTS" },
			new ConvertToWordsTestCase { Input = 111.0, Expected = "ONE HUNDRED AND ELEVEN DOLLARS AND ZERO CENTS" },
			new ConvertToWordsTestCase { Input = 121.0, Expected = "ONE HUNDRED AND TWENTY ONE DOLLARS AND ZERO CENTS" },
			new ConvertToWordsTestCase { Input = 123.11, Expected = "ONE HUNDRED AND TWENTY THREE DOLLARS AND ELEVEN CENTS" },
			new ConvertToWordsTestCase { Input = 123.45, Expected = "ONE HUNDRED AND TWENTY THREE DOLLARS AND FORTY FIVE CENTS" },
			new ConvertToWordsTestCase { Input = 1111.0, Expected = "ONE THOUSAND ONE HUNDRED AND ELEVEN DOLLARS AND ZERO CENTS" },
			new ConvertToWordsTestCase { Input = 11111.0, Expected = "ELEVEN THOUSAND ONE HUNDRED AND ELEVEN DOLLARS AND ZERO CENTS" },
			new ConvertToWordsTestCase { Input = 111111.0, Expected = "ONE HUNDRED AND ELEVEN THOUSAND ONE HUNDRED AND ELEVEN DOLLARS AND ZERO CENTS" },
			new ConvertToWordsTestCase { Input = 1111111.0, Expected = "ONE MILLION ONE HUNDRED AND ELEVEN THOUSAND ONE HUNDRED AND ELEVEN DOLLARS AND ZERO CENTS" },
		};
	}

	public static TheoryData<ConvertToWordsTestCase> ConvertOnesTheoryData()
	{
		return new()
		{
			new ConvertToWordsTestCase { Input = 1.0, Expected = "ONE DOLLAR AND ZERO CENTS" },
			new ConvertToWordsTestCase { Input = 2.0, Expected = "TWO DOLLARS AND ZERO CENTS" },
			new ConvertToWordsTestCase { Input = 3.0, Expected = "THREE DOLLARS AND ZERO CENTS" },
			new ConvertToWordsTestCase { Input = 4.0, Expected = "FOUR DOLLARS AND ZERO CENTS" },
			new ConvertToWordsTestCase { Input = 5.0, Expected = "FIVE DOLLARS AND ZERO CENTS" },
			new ConvertToWordsTestCase { Input = 6.0, Expected = "SIX DOLLARS AND ZERO CENTS" },
			new ConvertToWordsTestCase { Input = 7.0, Expected = "SEVEN DOLLARS AND ZERO CENTS" },
			new ConvertToWordsTestCase { Input = 8.0, Expected = "EIGHT DOLLARS AND ZERO CENTS" },
			new ConvertToWordsTestCase { Input = 9.0, Expected = "NINE DOLLARS AND ZERO CENTS" },
		};
	}

	public static TheoryData<ConvertToWordsTestCase> ConvertTeensTheoryData()
	{
		return new()
		{
			new ConvertToWordsTestCase { Input = 11.0, Expected = "ELEVEN DOLLARS AND ZERO CENTS" },
			new ConvertToWordsTestCase { Input = 12.0, Expected = "TWELVE DOLLARS AND ZERO CENTS" },
			new ConvertToWordsTestCase { Input = 13.0, Expected = "THIRTEEN DOLLARS AND ZERO CENTS" },
			new ConvertToWordsTestCase { Input = 14.0, Expected = "FOURTEEN DOLLARS AND ZERO CENTS" },
			new ConvertToWordsTestCase { Input = 15.0, Expected = "FIFTEEN DOLLARS AND ZERO CENTS" },
			new ConvertToWordsTestCase { Input = 16.0, Expected = "SIXTEEN DOLLARS AND ZERO CENTS" },
			new ConvertToWordsTestCase { Input = 17.0, Expected = "SEVENTEEN DOLLARS AND ZERO CENTS" },
			new ConvertToWordsTestCase { Input = 18.0, Expected = "EIGHTEEN DOLLARS AND ZERO CENTS" },
			new ConvertToWordsTestCase { Input = 19.0, Expected = "NINETEEN DOLLARS AND ZERO CENTS" },
		};
	}

	public static TheoryData<ConvertToWordsTestCase> ConvertTensTheoryData()
	{
		return new()
		{
			new ConvertToWordsTestCase { Input = 10.0, Expected = "TEN DOLLARS AND ZERO CENTS" },
			new ConvertToWordsTestCase { Input = 20.0, Expected = "TWENTY DOLLARS AND ZERO CENTS" },
			new ConvertToWordsTestCase { Input = 30.0, Expected = "THIRTY DOLLARS AND ZERO CENTS" },
			new ConvertToWordsTestCase { Input = 40.0, Expected = "FORTY DOLLARS AND ZERO CENTS" },
			new ConvertToWordsTestCase { Input = 50.0, Expected = "FIFTY DOLLARS AND ZERO CENTS" },
			new ConvertToWordsTestCase { Input = 60.0, Expected = "SIXTY DOLLARS AND ZERO CENTS" },
			new ConvertToWordsTestCase { Input = 70.0, Expected = "SEVENTY DOLLARS AND ZERO CENTS" },
			new ConvertToWordsTestCase { Input = 80.0, Expected = "EIGHTY DOLLARS AND ZERO CENTS" },
			new ConvertToWordsTestCase { Input = 90.0, Expected = "NINETY DOLLARS AND ZERO CENTS" },
			new ConvertToWordsTestCase { Input = 11.0, Expected = "ELEVEN DOLLARS AND ZERO CENTS" },
			new ConvertToWordsTestCase { Input = 21.0, Expected = "TWENTY ONE DOLLARS AND ZERO CENTS" },
			new ConvertToWordsTestCase { Input = 31.0, Expected = "THIRTY ONE DOLLARS AND ZERO CENTS" },
			new ConvertToWordsTestCase { Input = 41.0, Expected = "FORTY ONE DOLLARS AND ZERO CENTS" },
			new ConvertToWordsTestCase { Input = 51.0, Expected = "FIFTY ONE DOLLARS AND ZERO CENTS" },
			new ConvertToWordsTestCase { Input = 61.0, Expected = "SIXTY ONE DOLLARS AND ZERO CENTS" },
			new ConvertToWordsTestCase { Input = 71.0, Expected = "SEVENTY ONE DOLLARS AND ZERO CENTS" },
			new ConvertToWordsTestCase { Input = 81.0, Expected = "EIGHTY ONE DOLLARS AND ZERO CENTS" },
			new ConvertToWordsTestCase { Input = 91.0, Expected = "NINETY ONE DOLLARS AND ZERO CENTS" },
		};
	}

	public static TheoryData<ConvertToWordsTestCase> ConvertPowersOfThousandTheoryData()
	{
		return new()
		{
			new ConvertToWordsTestCase { Input = 10.0, Expected = "TEN DOLLARS AND ZERO CENTS" },
			new ConvertToWordsTestCase { Input = 100.0, Expected = "ONE HUNDRED DOLLARS AND ZERO CENTS" },
			new ConvertToWordsTestCase { Input = 1000.0, Expected = "ONE THOUSAND DOLLARS AND ZERO CENTS" },
			new ConvertToWordsTestCase { Input = 10000.0, Expected = "TEN THOUSAND DOLLARS AND ZERO CENTS" },
			new ConvertToWordsTestCase { Input = 100000.0, Expected = "ONE HUNDRED THOUSAND DOLLARS AND ZERO CENTS" },
			new ConvertToWordsTestCase { Input = 1000000.0, Expected = "ONE MILLION DOLLARS AND ZERO CENTS" },
			new ConvertToWordsTestCase { Input = 1000000000.0, Expected = "ONE BILLION DOLLARS AND ZERO CENTS" },
			new ConvertToWordsTestCase { Input = 1000000001.0, Expected = "ONE BILLION AND ONE DOLLARS AND ZERO CENTS" },
			new ConvertToWordsTestCase { Input = 1000000011.0, Expected = "ONE BILLION AND ELEVEN DOLLARS AND ZERO CENTS" },
			new ConvertToWordsTestCase { Input = 1000000111.0, Expected = "ONE BILLION ONE HUNDRED AND ELEVEN DOLLARS AND ZERO CENTS" },
			new ConvertToWordsTestCase { Input = 1000001111.0, Expected = "ONE BILLION ONE THOUSAND ONE HUNDRED AND ELEVEN DOLLARS AND ZERO CENTS" },
			new ConvertToWordsTestCase { Input = 1000011111.0, Expected = "ONE BILLION ELEVEN THOUSAND ONE HUNDRED AND ELEVEN DOLLARS AND ZERO CENTS" },
			new ConvertToWordsTestCase { Input = 1000111111.0, Expected = "ONE BILLION ONE HUNDRED AND ELEVEN THOUSAND ONE HUNDRED AND ELEVEN DOLLARS AND ZERO CENTS" },
			new ConvertToWordsTestCase { Input = 1001111111.0, Expected = "ONE BILLION ONE MILLION ONE HUNDRED AND ELEVEN THOUSAND ONE HUNDRED AND ELEVEN DOLLARS AND ZERO CENTS" },
			new ConvertToWordsTestCase { Input = 1011111111.0, Expected = "ONE BILLION ELEVEN MILLION ONE HUNDRED AND ELEVEN THOUSAND ONE HUNDRED AND ELEVEN DOLLARS AND ZERO CENTS" },
			new ConvertToWordsTestCase { Input = 1111111111.0, Expected = "ONE BILLION ONE HUNDRED AND ELEVEN MILLION ONE HUNDRED AND ELEVEN THOUSAND ONE HUNDRED AND ELEVEN DOLLARS AND ZERO CENTS" },
			new ConvertToWordsTestCase { Input = 1010101010.10, Expected = "ONE BILLION TEN MILLION ONE HUNDRED AND ONE THOUSAND AND TEN DOLLARS AND TEN CENTS" },
		};
	}
	public static TheoryData<ConvertToWordsTestCase> ConvertDecimalsTheoryData() {
		return new()
		{
			new ConvertToWordsTestCase{ Input = 0.0, Expected = "ZERO DOLLARS AND ZERO CENTS" },
			new ConvertToWordsTestCase{ Input = 0.1, Expected = "ZERO DOLLARS AND TEN CENTS" },
			new ConvertToWordsTestCase{ Input = 0.01, Expected = "ZERO DOLLARS AND ONE CENT" },
			new ConvertToWordsTestCase{ Input = 0.11, Expected = "ZERO DOLLARS AND ELEVEN CENTS" },
			new ConvertToWordsTestCase{ Input = 0.111, Expected = "ZERO DOLLARS AND ELEVEN CENTS" },
			new ConvertToWordsTestCase{ Input = 10.0, Expected = "TEN DOLLARS AND ZERO CENTS" },
			new ConvertToWordsTestCase{ Input = 00000.1, Expected = "ZERO DOLLARS AND TEN CENTS" },
			new ConvertToWordsTestCase{ Input = 00000.01, Expected = "ZERO DOLLARS AND ONE CENT" },
			new ConvertToWordsTestCase{ Input = 00000000.00000, Expected = "ZERO DOLLARS AND ZERO CENTS" },
		};
	}

	public static TheoryData<ConvertToWordsTestCase> ConvertGrammaticalTheoryData()
	{
		return new()
		{
			new ConvertToWordsTestCase { Input = 1.0, Expected = "ONE DOLLAR AND ZERO CENTS" },
			new ConvertToWordsTestCase { Input = 2.0, Expected = "TWO DOLLARS AND ZERO CENTS" },
			new ConvertToWordsTestCase { Input = 103.0, Expected = "ONE HUNDRED AND THREE DOLLARS AND ZERO CENTS" },
			new ConvertToWordsTestCase { Input = 130.0, Expected = "ONE HUNDRED AND THIRTY DOLLARS AND ZERO CENTS" },
			new ConvertToWordsTestCase { Input = 1000001.0, Expected = "ONE MILLION AND ONE DOLLARS AND ZERO CENTS" },
			new ConvertToWordsTestCase { Input = 1001001.0, Expected = "ONE MILLION ONE THOUSAND AND ONE DOLLARS AND ZERO CENTS" },
			new ConvertToWordsTestCase { Input = 1001011.0, Expected = "ONE MILLION ONE THOUSAND AND ELEVEN DOLLARS AND ZERO CENTS" },
		};
	}

	[Theory]
	[MemberData(nameof(ConvertBaseTheoryData))]
	[MemberData(nameof(ConvertOnesTheoryData))]
	[MemberData(nameof(ConvertTeensTheoryData))]
	[MemberData(nameof(ConvertTensTheoryData))]
	[MemberData(nameof(ConvertPowersOfThousandTheoryData))]
	[MemberData(nameof(ConvertDecimalsTheoryData))]
	[MemberData(nameof(ConvertGrammaticalTheoryData))]
	public async Task ConvertNumbersToWords(ConvertToWordsTestCase testCase)
	{
		var convertedNumber = await _englishNumbersToWordsService.ConvertDollarValueToString(testCase.Input);
		Assert.Equal(testCase.Expected, convertedNumber);
	}
}
