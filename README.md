# TechnologyOne Code Challenge

## Running

**Docker** :

```shell
docker build -t tim-techone-challenge .
docker run -p 8000:80 tim-techone-challenge
```

**Docker Compose**:

```shell
docker compose up
```

## Possible Methods

**Recursion**:

Recursion is definitely a viable option and probably the first one to come to mind when approaching this problem. However, I personally don't like using recursion in any serverside solution (or any where performance has any impactr). The reason for this is not because I believe recursion to be unperformant, but because I believe that a recursive solution can easily be broken in a system or difficult to understand / read for other developers.

Recursion is simple enough as soon as you have figured out nice patterns to easily create and visualise recursion, but this is something typically only experienced devs are comfortable with, or at least experienced devs will make less mistakes. I don't want to write a solution that doesn't make sense to anyone who may be updating, fixing or replicating the feature; unless necessary.

For these reasons I have decided not to go with a recursive solution.

**Stack data structure**:

Another tool that could be of great use in this situation is a stack, due to the way in which reading numbers in English works. I could leverage a stack due to its first in last out property meaning the string can be composed starting from the smallest part of the number and the then stitched together in reverse.

Again, I believe this is potentially over complicating the problem when we could just use a string builder and prepend to it.

## Solution

So I created an Asp.Net application with the React.Net template (for speed and convenience).

#### Server

I created an `EnglishNumbersToWordsService` to perform the work required to complete the feature, and I also created a `CurrencyController` with a route called `ConvertToLongCurrency` which takes a `double` as a parameter.

The problem basically came down to converting a 3 digit number to words and then repeating until the whole number had been written. So to achieve this I create three main functions, to do just that:

```csharp
/**
* Convert a Dollar amount to words.
*
* @param value The dollar amount to convert.
* @return The dollar amount in words.
*/
public async Task<string> ConvertDollarValueToString(double value)
{
   ...
}
```

```csharp
/**
* Converts a number to words.
*
* @param number the number to convert
* @return the number in words
*/
private string NumberToWords(long number)
{
   ...
}
```

```csharp
/**
* Converts a three digit number to words
*
* @param number The three digit number to convert
* @return The number in words
*/
private string ThreeDigitNumberToWords(short number)
{
   ...
}
```

#### Client

This was pretty simple, I just made a component which debounced a number input and made a request whenever the number changed. I also made two supporting hooks: `useDebouncedState` and `useAsyncEffect`.

```react
export const CurrencyLongForm: React.FC = () => {
	const [value, setValue] = useState(0);
	const [loading, setLoading] = useState(false);
	const [debouncedValue] = useDebouncedState(value, 500);

	const [currencyLongForm, setCurrencyLongForm] = useState('');

	useAsyncEffect(async () => {
		setLoading(true);
		try {
			const res = await fetch('/api/Currency/ConvertToLongCurrency/' + encodeURIComponent(debouncedValue));
			const data = await res.text();
			setCurrencyLongForm(data);
		} catch (e) {
			setCurrencyLongForm('');
		} finally {
			setLoading(false);
		}
	}, [debouncedValue]);

	const onChange: React.ChangeEventHandler<HTMLInputElement> = (e) => {
		setValue(e.target.valueAsNumber);
	}

	return (
		<div className={'currency-long-form-section'}>
			<input data-testid={'dollar-numeric-input'} type="number" value={value} onChange={onChange} />
			{loading ? <span>Loading...</span> : <span>{currencyLongForm}</span>}
		</div>
	);
}
```

## Test Plan

**Clientside**:

There is very little functionality here so testing is mostly to ensure that it doesn't change or regress without anyone knowing.

1. Snapshot of any visual components
2. Mock the serverside fetch
3. Ensure the input works as intended

**Serverside**:

This is where most of the feature lies and as such where most of the testing should occur.

1. All of the standard cases for numbers e.g. 0 - 9; 10, 20, ...; and 1000, 1000000, ...
2. Pluralisation cases asserting that the correct pluralisation is used e.g. 1.1 and 2.2
3. Decimal cases / user input cases e.g. 1.1, 1.11, 1.111
4. All grammatical edge cases, e.g. 101.0, 100001.0, 100000.0 (making sure 'and's are being correctly used)
